/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.model.message;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/***
 * 文本类型
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 9:57
 */
public class DingMessageText extends DingMessage {

    private final String content;

    /**
     * 消息内容，建议500字符以内
     * @param content
     */
    public DingMessageText(String content) {
        super(MESSAGE_TEXT);
        this.content = content;
    }

    @Override
    public String toString() {
        //得到文本消息类JSON字符串
        Map<String,Object> map=getDefaultMap();
        Map<String,Object> text=new HashMap<>();
        text.put("content",content);
        map.put("text",text);
        return JSON.toJSONString(map);
    }
}
