/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.model.message;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/***
 *
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 10:46
 */
public class DingMessageLink extends DingMessage {

    private final String messageUrl;
    private final String picUrl;
    private final String title;
    private final String text;

    public DingMessageLink(String messageUrl, String picUrl, String title, String text) {
        super(MESSAGE_LINK);
        this.messageUrl = messageUrl;
        this.picUrl = picUrl;
        this.title = title;
        this.text = text;
    }

    @Override
    public String toString() {
        //得到文本消息类JSON字符串
        Map<String,Object> map=getDefaultMap();
        Map<String,Object> link=new HashMap<>();
        link.put("messageUrl",messageUrl);
        link.put("picUrl",picUrl);
        link.put("title",title);
        link.put("text",text);

        map.put("link",link);
        return JSON.toJSONString(map);
    }
}
