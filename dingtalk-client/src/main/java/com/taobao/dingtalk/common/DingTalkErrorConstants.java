/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.common;

/***
 *
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 10:17
 */
public class DingTalkErrorConstants {

    /**
     * 系统繁忙-服务器暂不可用，建议稍候再重试1次，最多重试3次
     */
    public static final Long ERROR_CODE_BUSY=-1L;

    /**
     * 请求成功
     */
    public static final Long ERROR_CODE_SUCCESS=0L;


}
