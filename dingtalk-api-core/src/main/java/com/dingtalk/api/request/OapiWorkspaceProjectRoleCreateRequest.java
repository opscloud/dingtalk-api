package com.dingtalk.api.request;

import java.util.List;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.internal.util.RequestCheckUtils;
import com.taobao.api.TaobaoObject;
import java.util.Map;
import java.util.List;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.dingtalk.api.DingTalkConstants;
import com.taobao.api.Constants;
import com.taobao.api.internal.util.TaobaoHashMap;
import com.taobao.api.internal.util.TaobaoUtils;
import com.taobao.api.internal.util.json.JSONWriter;
import com.dingtalk.api.response.OapiWorkspaceProjectRoleCreateResponse;

/**
 * TOP DingTalk-API: dingtalk.oapi.workspace.project.role.create request
 * 
 * @author top auto create
 * @since 1.0, 2019.09.16
 */
public class OapiWorkspaceProjectRoleCreateRequest extends BaseTaobaoRequest<OapiWorkspaceProjectRoleCreateResponse> {
	
	

	/** 
	* 创建角色参数
	 */
	private String tags;

	public void setTags(String tags) {
		this.tags = tags;
	}

	public void setTags(List<OpenTagCreateDto> tags) {
		this.tags = new JSONWriter(false,false,true).write(tags);
	}

	public String getTags() {
		return this.tags;
	}

	public String getApiMethodName() {
		return "dingtalk.oapi.workspace.project.role.create";
	}

	private String topResponseType = Constants.RESPONSE_TYPE_DINGTALK_OAPI;

     public String getTopResponseType() {
        return this.topResponseType;
     }

     public void setTopResponseType(String topResponseType) {
        this.topResponseType = topResponseType;
     }

     public String getTopApiCallType() {
        return DingTalkConstants.CALL_TYPE_OAPI;
     }

     private String topHttpMethod = DingTalkConstants.HTTP_METHOD_POST;

     public String getTopHttpMethod() {
     	return this.topHttpMethod;
     }

     public void setTopHttpMethod(String topHttpMethod) {
        this.topHttpMethod = topHttpMethod;
     }

     public void setHttpMethod(String httpMethod) {
         this.setTopHttpMethod(httpMethod);
     }

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("tags", this.tags);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<OapiWorkspaceProjectRoleCreateResponse> getResponseClass() {
		return OapiWorkspaceProjectRoleCreateResponse.class;
	}

	public void check() throws ApiRuleException {
		RequestCheckUtils.checkObjectMaxListSize(tags, 20, "tags");
	}
	
	/**
	 * 创建角色参数
	 *
	 * @author top auto create
	 * @since 1.0, null
	 */
	public static class OpenTagCreateDto extends TaobaoObject {
		private static final long serialVersionUID = 5895249172282653285L;
		/**
		 * 角色名
		 */
		@ApiField("name")
		private String name;
	
		public String getName() {
			return this.name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	

}